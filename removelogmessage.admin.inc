<?php

/**
 * @file
 * Form builder; displays the removelogmessage settings form.
 */
function removelogmessage_settings($form, &$form_state) {
  $content_types = node_type_get_types();
  $roles = user_roles();

  foreach ($content_types as $type => $values) {
    $form[$type] = array(
      '#type' => 'fieldset',
      '#title' => t($type . ' Settings'),
      '#access' => user_access('administer remove log message'),
      '#weight' => -1,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form[$type]['roles'] = array(
      '#type' => 'fieldset',
      '#title' => t('For these Roles:'),
      '#description' => t('Checking no boxes is the same as checking all boxes.'),
      '#weight' => 0,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    foreach ($roles as $roleid => $rolename) {
      $form[$type]['roles']['rlm_role_' . $type . '_' . $roleid] = array(
        '#type' => 'checkbox',
        '#title' => $rolename,
        '#default_value' => variable_get('rlm_role_' . $type . '_' . $roleid, 0),
      );
    }
    $form[$type]['actions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Do the Following:'),
      '#weight' => 0,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form[$type]['actions']['rlm_log_' . $type] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide the log.'),
      '#default_value' => variable_get('rlm_log_' . $type, 0),
    );
    $form[$type]['actions']['rlm_revisions_' . $type] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide the whole revision fieldset.'),
      '#default_value' => variable_get('rlm_revisions_' . $type, 0),
    );
  }
  return system_settings_form($form);
}
